
public class Lotto {

	private static int[] LOTTO_ZAHLEN = {3, 7, 12, 18, 37, 42};
	
	public static void main(String[] args) {
		
		if(beinhaltetZahl(12))
			System.out.println("Die Zahl 12 ist in der Ziehung enthalten");
		else
			System.out.println("Die Zahl 12 ist nicht in der Ziehung enthalten");
		
		if(beinhaltetZahl(13))
			System.out.println("Die Zahl 13 ist in der Ziehung enthalten");
		else
			System.out.println("Die Zahl 13 ist nicht in der Ziehung enthalten");
	}
	
	private static boolean beinhaltetZahl(int zahl) {
		
		boolean beinhaltet = false;
		
		for(int lottoZahl : LOTTO_ZAHLEN) {
			
			if(lottoZahl == zahl) {
				beinhaltet = true;
				break;
			}
		}
		
		return beinhaltet;
	}
	
}


public class Main {

	public static void main(String[] args) {
		
		Raumschiff klingonen = new Raumschiff();
		Raumschiff romulaner = new Raumschiff();
		Raumschiff vulkanier = new Raumschiff();
		
		Ladung ladung1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung ladung2 = new Ladung("Borg-Schrott", 5);
		Ladung ladung3 = new Ladung("Rote Materie", 2);
		Ladung ladung4 = new Ladung("Forschungssonde", 35);
		Ladung ladung5 = new Ladung("Bat'leth Klingonen Schwert", 200);
		Ladung ladung6 = new Ladung("Plasma-Waffe", 50);
		Ladung ladung7 = new Ladung("Photonentorpedo", 3);
		
		klingonen.addLadung(ladung1);
		klingonen.addLadung(ladung5);
		
		romulaner.addLadung(ladung2);
		romulaner.addLadung(ladung3);
		romulaner.addLadung(ladung6);
		
		vulkanier.addLadung(ladung4);
		vulkanier.addLadung(ladung7);
		
	}
	
}

import java.util.Scanner;

public class UserInput {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Herzlich willkommen, wie darf ich Sie nennen?");
		
		String name = scanner.nextLine();
		
		System.out.println(name + ", das ist ja ein sch�ner Name!");
		System.out.println("Verr�tst du mir noch wie alt du bist?");
		
		int alter = scanner.nextInt();
		
		System.out.println("Wunderbar!");
		System.out.println(name + ", ich muss dir leider mitteilen, dass du mit " + alter + " noch zu jung f�r diese Anwendung bist.");
		System.out.println("Das Programm wird sich nun schlie�en");
		System.exit(0);
	}
	
}

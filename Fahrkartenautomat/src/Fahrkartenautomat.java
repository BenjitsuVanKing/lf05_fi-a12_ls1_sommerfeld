import java.util.Scanner;

class Fahrkartenautomat
{
	
	
    public static void main(String[] args)
    {
    	
       Scanner tastatur = new Scanner(System.in);
       Fahrkartenautomat fahrkartenautomat = new Fahrkartenautomat();
       
       while(true) {
           
           double zuZahlenderBetrag = fahrkartenautomat.fahrkartenbestellungErfassen(tastatur);
           
           fahrkartenautomat.fahrkartenBezahlen(tastatur, zuZahlenderBetrag);
           fahrkartenautomat.fahrkartenAusgeben();
     
           fahrkartenautomat.rueckgeldAusgeben(zuZahlenderBetrag);
           
           System.out.println("\nVergessen Sie nicht, die Fahrscheine\n"+
                   "vor jeweiligen Fahrtantritt entwerten zu lassen!\n"+
                   "Wir w�nschen Ihnen eine gute Fahrt. \n \n");
       }

    }
    
    private double eingezahlterGesamtbetrag;
    
    public void fahrkartenBezahlen(Scanner tastatur, double betrag) {
    	
        eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < betrag)
        {
     	   System.out.printf("Noch zu zahlen: %.2f Euro \n", (betrag - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
     	   double eingeworfeneM�nze = tastatur.nextDouble();
     	   
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
        }
    }
    
    public void fahrkartenAusgeben() {
    	
        System.out.println("\nFahrscheine werden ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			warte(250);
 		} catch (InterruptedException e) {
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    }
    
    public void rueckgeldAusgeben(double zuZahlenderBetrag) {
    	
        double r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if(r�ckgabebetrag > 0.0)
        {
     	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f Euro \n", r�ckgabebetrag);
     	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
            {
            	muenzeAusgeben(2, "EURO");
 	          	r�ckgabebetrag -= 2.0;
            }
            while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
            {
            	muenzeAusgeben(1, "EURO");
            	r�ckgabebetrag -= 1.0;
            }
            while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
            {
            	muenzeAusgeben(50, "CENT");
            	r�ckgabebetrag -= 0.5;
            }
            while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
            {
            	muenzeAusgeben(20, "CENT");
  	          	r�ckgabebetrag -= 0.2;
            }
            while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
            {
            	muenzeAusgeben(10, "CENT");
            	r�ckgabebetrag -= 0.1;
            }
            while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
            {
            	muenzeAusgeben(5, "CENT");
  	          	r�ckgabebetrag -= 0.05;
            }
        }
    }
    
    public double fahrkartenbestellungErfassen(Scanner tastatur) {
    	
        System.out.println("W�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus:");
        System.out.println("Einzelfahrschein Regeltarif AB [2,90 EUR] (1)");
        System.out.println("Tageskarte Regeltarif AB [8,60 EUR] (2)");
        System.out.println("Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
        
        int eingabe = tastatur.nextInt();
        
        while(eingabe < 1 || eingabe > 3) {
        	
        	System.out.println(">>falsche Eingabe<<");
        	eingabe = tastatur.nextInt();
        }
        
        double zuZahlenderBetrag = 0;
        
        switch(eingabe) {
        	
        case 1:
        	zuZahlenderBetrag = 2.90;
        	break;
        
        case 2:
        	zuZahlenderBetrag = 8.60;
        	break;
        	
        case 3:
        	zuZahlenderBetrag = 23.50;
        	break;
        }
        
        int tickets = korrektiereTicketAnzahl(tastatur);
        return zuZahlenderBetrag*tickets;
    }
    
    private int korrektiereTicketAnzahl(Scanner tastatur) {
    	
        System.out.print("W�hlen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
        int tickets = tastatur.nextInt();
        
        if(tickets <= 0 || tickets > 10)
        	return korrektiereTicketAnzahl(tastatur);
        
        return tickets;
    }
    
    // Millisekunden sind immer long
    private void warte(long ms) throws InterruptedException {
    	Thread.sleep(ms);
    }
    
    private void muenzeAusgeben(int betrag, String einheit) {
    	System.out.println(betrag + " " + einheit);
    }
    
}
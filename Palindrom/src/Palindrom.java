import java.util.Arrays;
import java.util.Scanner;

public class Palindrom {
	
	public static void main(String[] args) {
	
		Scanner tastatur = new Scanner(System.in);
		String[] zeichen = new String[5];
		
		System.out.println("Bitte geben Sie 5 Zeichen nacheinander ein.");
		
		int index = 0;
		while(index < zeichen.length)
			zeichen[index++] = tastatur.next();
		
		String[] reversed_zeichen = new String[zeichen.length];
		for(int i = 1; i <= zeichen.length; i++)
			reversed_zeichen[i-1] = zeichen[zeichen.length-i];
		
		System.out.println("Richtige Reihenfolge: " + Arrays.toString(zeichen));
		System.out.println("Reversed Reihenfolge: " + Arrays.toString(reversed_zeichen));
	}

}

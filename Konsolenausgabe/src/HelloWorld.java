
import static java.lang.System.out;

public class HelloWorld {

	public static void main(String[] args) {
		
		out.print("Das ist ein ausgedachter Satz. ");
		out.print("Oh wow! Noch ein ausgedachter Satz! \n");
		out.print("Und der Compiler sagte: \"Ich hab gerade einen Zeilenumbruch vernommen\". \n");
		
		final int anzahl_der_sysouts = 4;
		out.print("Und genau jetzt in diesem Moment, betr�gt die Anzahl der Sysouts " + anzahl_der_sysouts);
		
		out.print("\n\n");
		
//		String stern = "*";
//		for(int i = 14; i < 21; i++) {
//			
//			if(i != 14) 
//				stern+="**";
//			out.printf("%" + i + "s\n", stern);
//		}
//		
//		out.printf("%15s\n", "***");
//		out.printf("%15s\n", "***");
		
		out.printf("%14s\n", "*");
		out.printf("%15s\n", "***");
		out.printf("%16s\n", "*****");
		out.printf("%17s\n", "*******");
		out.printf("%18s\n", "*********");
		out.printf("%19s\n", "***********");
		out.printf("%20s\n", "*************");
		out.printf("%15s\n", "***");
		out.printf("%15s\n", "***");
		
		out.printf("%.2f\n", 22.4234234);
		out.printf("%.2f\n", 111.2222);
		out.printf("%.2f\n", 4.0);
		out.printf("%.2f\n", 10000000.551);
		out.printf("%.2f\n", 97.34);
		
		out.print("\n");
		
		formatRechnung("0!", "", "1");
		formatRechnung("1!", "1", "1");
		formatRechnung("2!", "1 * 2", "2");
		formatRechnung("3!", "1 * 2 * 3", "6");
		formatRechnung("4!", "1 * 2 * 3 * 4", "24");
		formatRechnung("5!", "1 * 2 * 3 * 4 * 5", "120");
		
		out.printf("%-12s|", "Fahrenheit");
		out.printf("%10s\n", "Celsius");
		out.println("-------------------------");
		formatFahrenheitCelsius(-20, -28.8889);
		formatFahrenheitCelsius(-10, -23.3333);
		formatFahrenheitCelsius(0, -17.7778);
		formatFahrenheitCelsius(20, -6.6667);
		formatFahrenheitCelsius(30, -1.1111);
		
		//Hier endet unser Programm, nachdem es unsere Anweisungen erf�llt hat
	}
	
	private static void formatFahrenheitCelsius(int fahrenheit, double celsius) {
		
		out.printf("%-12s|", fahrenheit);
		out.printf("%10.2f\n", celsius);
	}
	
	private static void formatRechnung(String s, String s1, String s2) {
		
		out.printf("%-5s", s);
		out.print("= ");
		out.printf("%-19s", s1);
		out.print("= ");
		out.printf("%4s\n", s2);
	}
	
}

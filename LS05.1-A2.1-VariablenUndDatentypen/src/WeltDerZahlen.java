/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie d�rfen nicht die Namen der Variablen ver�ndern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Benjamin Sommerfeld >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
	  
	    /*  *********************************************************
	    
      Zuerst werden die Variablen mit den Werten festgelegt!
 
 *********************************************************** */
 // Im Internet gefunden ?
 // Die Anzahl der Planeten in unserem Sonnesystem                    
	  int anzahlPlaneten = 8;
 
 // Anzahl der Sterne in unserer Milchstra�e
	  long anzahlSterne = 400_000_000_000L;
 
 // Wie viele Einwohner hat Berlin?
	  int bewohnerBerlin = 3_645_000;
 
 // Wie alt bist du?  Wie viele Tage sind das?
 
	  int alterTage = 18*365;
 
 // Wie viel wiegt das schwerste Tier der Welt?
 // Schreiben Sie das Gewicht in Kilogramm auf!
	  int gewichtKilogramm = 150_000;
 
 // Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat?
	  int flaecheGroessteLand = 17_098_242;
 
 // Wie gro� ist das kleinste Land der Erde?
 
	  double flaecheKleinsteLand = 0.44;
 
 /*  *********************************************************
 
      Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
 
 *********************************************************** */
 
 System.out.println("Anzahl der Planeten: " + anzahlPlaneten);
 
 System.out.println("Anzahl der Sterne: " + anzahlSterne);
 
 System.out.println("Anzahl der Bewohner in Berlin: " + bewohnerBerlin);
 
 System.out.println("Mein Alter in Tagen: " + alterTage);
 
 System.out.println("Gewicht des schwersten Tieres der Welt: " + gewichtKilogramm);
 
 System.out.println("Fl�che des gr��ten Landes der Welt: " + flaecheGroessteLand);
 
 System.out.println("Fl�che des kleinsten Landes der Welt: " + flaecheKleinsteLand);
 
 System.out.println(" *******  Ende des Programms  ******* ");
	  
  }
}

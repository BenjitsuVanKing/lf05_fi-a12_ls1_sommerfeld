import java.util.function.Consumer;

public class ArraayAufgaben {
	
	public static void main(String[] args) {
		
		// "Zahlen" Aufgabe 1
		int[] zahlen = new int[10];
		
		for(int i = 0; i < zahlen.length; i++) 
			zahlen[i] = i;
		
		for(int i = 0; i < zahlen.length; i++)
			System.out.println(zahlen[i]);
		
		// "UngeradeZahlen" Aufgabe 2
		int[] ungeradeZahlen = new int[10];
		int index = 0;
		
		for(int i = 0; i < 20; i++) {
			
			if(i%2 == 0) continue;
			ungeradeZahlen[index++] = i;
		}
		
	}

}
